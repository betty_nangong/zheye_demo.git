/*
 * @Author: Betty
 * @Date: 2020-11-04 17:54:33
 * @LastEditors: Betty
 * @LastEditTime: 2020-11-18 22:15:32
 * @Description: file content
 */
declare module "*.vue" {
  import { defineComponent } from 'vue'
  const Component: ReturnType<typeof defineComponent>;
  export default Component
}
