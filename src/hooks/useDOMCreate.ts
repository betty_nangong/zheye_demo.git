/*
 * @Author: Betty
 * @Date: 2020-12-20 16:30:52
 * @LastEditors: Betty
 * @LastEditTime: 2020-12-20 16:56:47
 * @Description: 一个创建某个div的函数
 */
import { onUnmounted } from 'vue'

function useDOMCreate(name: string | HTMLDivElement) {
    if (typeof name === 'string') {
        const node = document.createElement('div')
        node.id = name
        document.body.appendChild(node)
        // 在组件被卸载后，从body中删除id为back的div
        onUnmounted(() => {
            document.body.removeChild(node)
        })
    } else if(typeof name === 'object') {
        document.body.appendChild(name)
        // 在组件被卸载后，从body中删除id为back的div
        onUnmounted(() => {
            document.body.removeChild(name)
        })
    }
}

export default useDOMCreate