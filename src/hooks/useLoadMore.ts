/*
 * @Author: Betty
 * @Date: 2021-03-06 22:48:29
 * @LastEditors: Betty
 * @LastEditTime: 2021-03-07 17:34:15
 * @Description: 加载更多
 */
import { ref, computed, ComputedRef } from 'vue'
import { useStore } from 'vuex'
// 定义加载更多的参数，当前第几页和每页多少个数据
interface LoadParams {
    currentPage: number;
    pageSize: number;
}
// 传入的参数是“要发起的请求”和“总个数”。由于总个数不确定，所以类型应该是ComptedRef，还有请求的参数
const useLoadMore = (actionName: string, total: ComputedRef<number>, params: LoadParams = {currentPage: 2, pageSize: 5}) => {
    const store = useStore()
    // 定义当前请求第几页，由于是会变化的，所以是ref
    const currentPage = ref(params.currentPage)
    // 定义发送请求时的参数
    // 如果我们直接写currentPage.value，拿到的就是它现在拿出来的值，并不是响应式的
    // 想要响应式的值，就要用计算属性来包裹它
    const requestParams = computed(() => ({
        currentPage: currentPage.value,
        pageSize: params.pageSize
    }))
    // 准备好要暴露出去的请求函数，让它来加载更多
    const loadMorePage = () => {
        store.dispatch(actionName, requestParams.value).then(() => {
            // 成功后，就把currentPage的值加1，让它下次请求下一页
            currentPage.value ++
        })
    }
    // 返回是否最后一页，这是个计算属性
    const isLastPage = computed(() => {
       // 总数除以每页多少个数据，再向上取整，得到共多少页。判断当前页是否等于总页数
       // 因为我们在判断之前，当前页已经自增了，判断的已经是下一页了，
       //  所以应该是“下一页”大于“总页数的时候，表示到最后了    
       return Math.ceil(total.value / params.pageSize) < currentPage.value
    })
    // 返回请求函数和isLastPage
    return {
        loadMorePage,
        isLastPage,
        currentPage
    }
}

export default useLoadMore