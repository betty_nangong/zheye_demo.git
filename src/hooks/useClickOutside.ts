/*
 * @Author: Betty
 * @Date: 2020-11-09 22:02:56
 * @LastEditors: Betty
 * @LastEditTime: 2020-11-10 22:48:02
 * @Description: 一个判断是否点击在某元素外的函数
 */
import { ref, onMounted, onUnmounted, Ref } from 'vue'
// 把逻辑封装到这个函数里去
// 参数是要判断的DOM的Ref对象（因为它的值从null变成了HTML节点，所以它的值发生了改变，所以要使用Ref，我们把参数声明成Ref，泛型为HTML元素或null)
const useClickOutside = (elementRef: Ref<HTMLElement | null>) => {
  // 返回值也是个ref对象，告诉用户是否点到了外面
  const isClickOutside = ref(false)
  // 创建判断是否点到外面的方法，这是一个点击事件处理函数
  const handler = (e: MouseEvent) => {
    // 假如它的值是HTML元素
    if (elementRef.value) {
      // 假如e.target被这个元素ref包含了，那么就返回false，并没有点到外面
      if (elementRef.value.contains(e.target as HTMLElement)) {
        isClickOutside.value = false
      } else {
        isClickOutside.value = true
      }
    }
  }
  // 在被挂载后就开始监听document上的点击事件，注意判断点击的是否在dropdown-item内
  onMounted(() => {
    document.addEventListener('click', handler)
  })
  // 在解除挂载后，记得移除点击事件，防止内存泄漏
  onUnmounted(() => {
    document.removeEventListener('click', handler)
  })
  return isClickOutside
}

export default useClickOutside
