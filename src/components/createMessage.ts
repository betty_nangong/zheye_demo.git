/*
 * @Author: Betty
 * @Date: 2021-01-05 20:47:55
 * @LastEditors: Betty
 * @LastEditTime: 2021-01-05 21:43:36
 * @Description: 用函数形式创建message组件
 */
import { createApp } from 'vue'
import Message from './Message.vue'
export type MessageType = 'success' | 'error' | 'default'

const createMessage = (message: string, type: MessageType, timeout = 2000) => {
    // 调用crateApp，生成Message组件的实例，
    // 第一个参数是Message组件本身，第二个参数是Message组件需要的prop值，恰好与我们的参数同名
    const messageInstance = createApp(Message, {
        message,
        type
    })
    // 创建一个供实例挂载的div
    const mountNode = document.createElement('div')
    document.body.appendChild(mountNode)
    // 把实例挂载到div上
    messageInstance.mount(mountNode)
    // 在一段时间后，卸载掉这个实例，并且删掉这个div
    setTimeout(() => {
      messageInstance.unmount(mountNode)
      document.body.removeChild(mountNode)
    }, timeout)
}

export default createMessage