/*
 * @Author: Betty
 * @Date: 2020-11-04 17:54:33
 * @LastEditors: Betty
 * @LastEditTime: 2021-03-21 17:44:14
 * @Description: file content
 */
import { createApp } from 'vue'
import App from './App.vue'
// 导入我们定义的router和store对象
import router from './router'
import store from './store'
// 导入axios
import axios from 'axios'
// 为axios配置全局的baseURL
axios.defaults.baseURL = 'http://apis.imooc.com/api/'
const icode = 'AC22E9BABB029A8D'
// 为axios设置拦截器，让请求加上icode参数
axios.interceptors.request.use(config => {
    // get请求的话，参数加到params里面去
    config.params= {
        ...config.params,
        icode
    }
    // 其他请求，添加到 body 中
    // 如果是上传文件，添加到 FormData 中
    if (config.data instanceof FormData) {
        config.data.append('icode', icode)
    } else {
    // 普通的 body 对象，添加到 data 中
        config.data = { ...config.data, icode }
    }
    // 在请求拦截器这里，我们调用setLoading，让loading在要发送请求时候是true
    store.commit('setLoading', true)
    // 把error的状态设置为false，以便遇到错误的时候改成true
    store.commit('setError', {status: false, message: ''})
    // 最后返回配置对象
    return config
})
// 响应拦截器
// 响应拦截器有两个回调函数，后一个是失败的回调，处理错误结果要在这里进行
axios.interceptors.response.use(config => {
    setTimeout(() => {
        // 在响应拦截器里，让loading变成false，表示拿到结果了，加载停止
        store.commit('setLoading', false)
    }, 100)
    return config
}, e => {
    if (e.response) {
        const { error } = e.response.data
        // 如果出错，那就发送mutation来修改
        store.commit('setError', {status: true, message: error})
        // 把loading停止掉
        store.commit('setLoading', false)
        return Promise.reject(error)
    }
    console.log(e.response)
    // 把loading停止掉
    store.commit('setLoading', false)
    return Promise.reject(e.response)
})
// 把router挂载上去
const app = createApp(App)
app.use(router).use(store).mount('#app')
