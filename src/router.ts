/*
 * @Author: Betty
 * @Date: 2020-11-27 22:57:03
 * @LastEditors: Betty
 * @LastEditTime: 2021-02-06 22:11:51
 * @Description: vue-router的使用
 */
// 导入vue-router相关的函数
import { createRouter, createWebHistory } from 'vue-router'
// 导入需要的页面
import Home from './views/Home.vue'
import Login from './views/Login.vue'
import ColumnDetail from './views/ColumnDetail.vue'
import CreatePost from './views/CreatePost.vue'
import SignUp from './views/SignUp.vue'
import PostDetail from './views/PostDetail.vue'
import store from './store'
import axios from 'axios'
// 创建routerHistory对象
const routerHistory = createWebHistory()
// 创建router对象
const router = createRouter({
  history: routerHistory,
  // routes里面的是我们的路由规则数组
  routes: [
    {
      path: '/',
      name: 'home',
      component: Home
    },
    {
      path: '/login',
      name: 'login',
      component: Login,
      // meta属性是元信息，这里可以写我们的标识信息
      meta: {
        redirectAlreayLogin: true
      }
    },
    {
      path: '/column/:id',
      name: 'column',
      component: ColumnDetail
    },
    {
      path: '/create',
      name: 'create',
      component: CreatePost,
      meta: {
        requiredLogin: true
      }
    },
    {
      path: '/register',
      name: 'register',
      component: SignUp
    },
    {
      path: '/post/:id',
      name: 'post',
      component: PostDetail
    }
  ]
})
// 在这里做一个全局的前置导航守卫
// 每个导航触发时，这些前置导航守卫会按顺序执行，是异步解析的，
// 导航在所有守卫resolve完之前一直在等待
// 传入一个回调函数，里面有3个参数，to即将前往的路由对象，form即将离开的路由对象
router.beforeEach((to, from, next) => {
  // 先把我们要用的数据结构出来
  const {user, token} = store.state
  const {redirectAlreayLogin, requiredLogin} = to.meta
  // 第一步，判断isLogin是否为true
  if (user.isLogin) {
    // 第二步，确认isLogin为true，那就判断是否现在要访问登录页,如果是则跳转首页，因为没必要再去登录
    if (redirectAlreayLogin) {
      next('/')
    } else {
      next() //如果不是，就继续访问你要访问的页面
    }
  } else {
    // 第二步，确认isLogin为false，那就判断是否有token
    if (token) {
      // 如果有token，那么就发送fetCurretUser请求
      // 设置请求首部的Authorization
      axios.defaults.headers.Authorization = `Bearer ${token}`
      // 发起获取用户信息的请求
      store.dispatch('fetchCurrentUser').then(() => {
        // 等到获取用户信息成功，就判断是否要去登录页，是就重定向到首页，否则继续
        if (redirectAlreayLogin) {
          next('/')
        } else {
          next()
        }
      }).catch(e => {//假如获取失败，那就先给个提示，然后跳转到登录页
        // 输出错误信息
        console.error(e)
        // 删掉有问题的token，注意不只是要在localStorage中删除token，在store中也要删除
        store.commit('logout')
        next('/login')
      })
    } else {
      // 如果确定没有token，那就是真的没有登录过或者已经退出了登录
      // 如果访问的是要求登录的页面，那么应该跳到登录，否则可以继续跳转
      if (requiredLogin) {
        next('/login')
      } else {
        next()
      }
    }
  }
})
// 导出router对象
export default router
