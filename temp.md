<!--
 * @Author: Betty
 * @Date: 2020-11-18 21:03:25
 * @LastEditors: Betty
 * @LastEditTime: 2020-11-18 21:14:09
 * @Description: file content
 -->
<validate-form @form-submit="onFormsubmit">
    <div class="mb-3">
        <label for="exampleInputEmail1" class="form-label">邮箱地址：</label>
    <!-- 绑定邮箱当前的值到ref的val属性，这样好判断值有没有问题，
    并且在blur事件的时候触发验证 -->
        <validate-input ref="inputRef" :rules="emailRules" v-model="emailVal" type="text" placeholder="请输入你的邮箱地址"></validate-input>
    </div>
    <div class="mb-3">
        <label for="exampleInputPassword1" class="form-label">密码：</label>
        <validate-input :rules="passwordRules" v-model="passwordVal" type="password" placeholder="请输入你的密码"></validate-input>
    </div>
    <!-- 为表单组件传入提交按钮v-slot:submit 缩写为#submit -->
    <template #submit>
        <span class="btn btn-danger">提交</span>
    </template>
</validate-form>

// 创建一个规则数组
const emailRules: RulesProp = [
    {
    type: 'required',
    message: '邮箱地址不能为空'
    },
    {
    type: 'email',
    message: '邮箱地址的格式不正确'
    },
    {
    type: 'minLength',
    value: 10,
    message: '邮箱地址的长度不小于10'
    },
    {
    type: 'maxLength',
    value: 16,
    message: '邮箱地址的长度不大于16'
    }
]

// 创建监听表单提交结果的函数
    const onFormsubmit = (result: boolean) => {
      // 拿到了子组件实例后，调用子组件的验证方法，来得到结果
      // 我们对这个结果数组调用every方法，来判断是否全true
      console.log(result)
      // 触发自定义事件，让里面的input组件的值全部清空
      emitter.emit('ready-clear-input')
    }

<!-- 我们定义一个专栏数组，存放测试数据 -->
const testData = [
  {
    id: 1,
    title: '南宫的专栏1',
    description: '这里是南宫的专栏1，欢迎你来看看'
  },
  {
    id: 2,
    title: '南宫的专栏2',
    description: '这里是南宫的专栏2，欢迎你来看看。我这里是不是很有意思',
    avatar: 'https://cn.vuejs.org/images/logo.png'
  },
  {
    id: 3,
    title: '南宫的专栏3',
    description: '这里是南宫的专栏3，欢迎你来看看',
    avatar: 'https://cn.vuejs.org/images/logo.png'
  },
  {
    id: 4,
    title: '南宫的专栏4',
    description: '这里是南宫的专栏4，欢迎你来看看',
    avatar: 'https://cn.vuejs.org/images/logo.png'
  }
]